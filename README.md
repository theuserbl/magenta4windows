# README #

In the "Downloads"-section are precompiled binaries of Googles Kernel Zircon  (formerly known as "Magenta") plus tools, which is part of its new Operating System called Fuchsia.

For more information have a look at
https://fuchsia.miraheze.org/wiki/Main_Page

For source code have a look at
https://fuchsia.googlesource.com/

For qemu the Windows-binaries at
https://qemu.weilnetz.de/w32/
https://qemu.weilnetz.de/w64/
are used